EESchema Schematic File Version 2
LIBS:multispec
LIBS:main-rescue
LIBS:main-cache
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
EELAYER 25 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 3 9
Title "Reflectance 1.0"
Date "2017-04-24"
Rev "1.0"
Comp "OurSci"
Comment1 "Zegarac Robert"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L TSV911 U13
U 1 1 5605D51F
P 8590 5185
F 0 "U13" H 8515 5260 40  0000 L CNN
F 1 "MIC6211" H 8515 5110 30  0000 L CNN
F 2 "multispeq:sot23-5" H 8490 5185 10  0001 C CNN
F 3 "MIC6211YM5-TR" H 8590 5185 10  0001 C CNN
F 4 "MIC6211YM5-TR" H 8590 5185 60  0001 C CNN "manf#"
	1    8590 5185
	1    0    0    1   
$EndComp
Text Notes 8690 4585 2    40   ~ 0
Detector -5V\n
$Comp
L MOSN-TSOP6 Q8
U 1 1 5605D544
P 9140 4760
F 0 "Q8" H 9390 4710 40  0000 C CNN
F 1 "MOSN" H 9390 4810 20  0000 C CNN
F 2 "multispeq:sc70-6L" H 9440 4610 30  0001 C CNN
F 3 "SIA416DJ-T1-GE3" H 9140 4760 60  0001 C CNN
F 4 "SIA416DJ-T1-GE3" H 9140 4760 60  0001 C CNN "manf#"
	1    9140 4760
	0    1    -1   0   
$EndComp
$Comp
L C_Small C28
U 1 1 5605D54C
P 9240 5035
F 0 "C28" H 9240 4985 50  0000 L CNN
F 1 "0.1u" H 9240 5085 50  0000 L CNN
F 2 "multispeq:c_0603" H 9240 5035 10  0001 C CNN
F 3 "C0603C104M5RACTU" H 9240 5035 10  0001 C CNN
F 4 "C0603C104M5RACTU" H 9240 5035 60  0001 C CNN "manf#"
	1    9240 5035
	-1   0    0    1   
$EndComp
$Comp
L C_Small C26
U 1 1 5605D554
P 8915 5185
F 0 "C26" H 8815 5235 50  0000 L CNN
F 1 "22p" H 8915 5110 50  0000 L CNN
F 2 "multispeq:c_0603" H 8915 5185 60  0001 C CNN
F 3 "06035A220JAT2A" H 8915 5185 60  0001 C CNN
F 4 "06035A220JAT2A" H 8915 5185 60  0001 C CNN "manf#"
	1    8915 5185
	0    1    1    0   
$EndComp
Text HLabel 9740 5685 2    39   Output ~ 0
GND-10
Text HLabel 9740 4660 2    39   Output ~ 0
-5Vc
Text HLabel 9000 7100 2    39   Output ~ 0
GND+5
Text HLabel 8920 6680 2    39   Output ~ 0
+5V
Text HLabel 7110 6680 0    39   Input ~ 0
+6V
Text HLabel 6990 4660 0    39   Input ~ 0
-7V
$Comp
L TL431LP U10
U 1 1 561124BE
P 7065 5235
F 0 "U10" H 6890 5385 40  0000 L BNN
F 1 "AN431AN-ATRG1" H 6815 5035 20  0000 L TNN
F 2 "multispeq:sot23r" H 7040 5110 30  0001 C CNN
F 3 "AN431AN-ATRG1" H 7065 5235 60  0001 C CNN
F 4 "AN431AN-ATRG1" H 7065 5235 60  0001 C CNN "manf#"
	1    7065 5235
	1    0    0    1   
$EndComp
Text Notes 7105 3895 0    100  ~ 0
Discrete linear regulators\nuse integrated equivalents instead
$Comp
L Q_NJFET_DGS Q6
U 1 1 5628473E
P 7840 4835
F 0 "Q6" V 7840 5035 50  0000 R CNN
F 1 "JFET" V 7790 4760 50  0000 R CNN
F 2 "multispeq:sot23" H 8040 4935 29  0001 C CNN
F 3 "MMBFJ201" H 7840 4835 60  0001 C CNN
F 4 "MMBFJ201" H 7840 4835 60  0001 C CNN "manf#"
	1    7840 4835
	0    1    -1   0   
$EndComp
$Comp
L R R40
U 1 1 563FAE43
P 9390 5510
F 0 "R40" V 9315 5510 50  0000 C CNN
F 1 "16K" V 9390 5510 50  0000 C CNN
F 2 "multispeq:r_0603" H 9390 5510 10  0001 C CNN
F 3 "RC1608J163CS" V 9390 5510 10  0001 C CNN
F 4 "RC1608J163CS" H 9390 5510 60  0001 C CNN "manf#"
	1    9390 5510
	1    0    0    -1  
$EndComp
$Comp
L R R39
U 1 1 563FFD37
P 9390 5010
F 0 "R39" V 9315 5010 50  0000 C CNN
F 1 "16K" V 9390 5010 50  0000 C CNN
F 2 "multispeq:r_0603" H 9390 5010 10  0001 C CNN
F 3 "RC1608J163CS" V 9390 5010 10  0001 C CNN
F 4 "RC1608J163CS" H 9390 5010 60  0001 C CNN "manf#"
	1    9390 5010
	1    0    0    -1  
$EndComp
$Comp
L R R34
U 1 1 56400912
P 7665 5235
F 0 "R34" V 7590 5235 50  0000 C CNN
F 1 "16K" V 7665 5235 50  0000 C CNN
F 2 "multispeq:r_0603" H 7665 5235 10  0001 C CNN
F 3 "RC1608J163CS" V 7665 5235 10  0001 C CNN
F 4 "RC1608J163CS" H 7665 5235 60  0001 C CNN "manf#"
	1    7665 5235
	0    1    1    0   
$EndComp
$Comp
L C_Small C31
U 1 1 563ADE9F
P 9540 4835
F 0 "C31" H 9390 4760 50  0000 L CNN
F 1 "100u" H 9390 4885 50  0000 L CNN
F 2 "multispeq:C_1210" H 9540 4835 10  0001 C CNN
F 3 "CL32A107MPVNNNE" H 9540 4835 10  0001 C CNN
F 4 "CL32A107MPVNNNE" H 9540 4835 60  0001 C CNN "manf#"
	1    9540 4835
	-1   0    0    1   
$EndComp
$Comp
L R R37
U 1 1 56557B45
P 8965 4960
F 0 "R37" V 8890 4960 50  0000 C CNN
F 1 "100" V 8965 4960 50  0000 C CNN
F 2 "multispeq:r_0603" H 8965 4960 10  0001 C CNN
F 3 "RC0603JR-07100RL" V 8965 4960 10  0001 C CNN
F 4 "RC0603JR-07100RL" H 8965 4960 60  0001 C CNN "manf#"
	1    8965 4960
	0    1    1    0   
$EndComp
Connection ~ 9540 4660
Wire Wire Line
	9540 5685 9540 4935
Connection ~ 8790 5685
Connection ~ 8490 4660
Wire Wire Line
	8490 4660 8490 5035
Wire Wire Line
	8490 5685 8490 5335
Wire Wire Line
	8790 4660 8790 4710
Connection ~ 8790 4660
Wire Wire Line
	9540 4660 9540 4735
Wire Wire Line
	9340 4660 9740 4660
Connection ~ 9390 4660
Wire Wire Line
	9390 4560 9340 4560
Wire Wire Line
	9390 4460 9340 4460
Connection ~ 9390 4560
Wire Wire Line
	9390 4360 9340 4360
Connection ~ 9390 4460
Wire Wire Line
	9390 4360 9390 4860
Connection ~ 8490 5685
Connection ~ 8490 4735
Wire Wire Line
	8790 5685 8790 4910
Wire Wire Line
	9390 5160 9390 5360
Connection ~ 9390 5335
Wire Wire Line
	9240 4835 9240 4935
Wire Wire Line
	9240 4835 9390 4835
Connection ~ 9390 4835
Wire Wire Line
	9240 5135 9240 5335
Connection ~ 9240 5335
Wire Wire Line
	9390 5685 9390 5660
Connection ~ 9390 5685
Connection ~ 9540 5685
Wire Wire Line
	8215 5335 9390 5335
Wire Wire Line
	6990 4660 8940 4660
Wire Wire Line
	7065 5685 9740 5685
Wire Wire Line
	7290 4735 7640 4735
Wire Wire Line
	7290 4735 7290 5435
Connection ~ 7290 5035
Wire Wire Line
	7065 5435 7065 5685
Wire Wire Line
	7290 5635 7290 5685
Connection ~ 7290 5685
Connection ~ 7290 5235
Wire Wire Line
	7815 5235 8290 5235
Wire Wire Line
	8090 5235 8090 5435
Connection ~ 8090 5235
Wire Wire Line
	8090 5635 8090 5685
Connection ~ 8090 5685
Wire Wire Line
	8215 5335 8215 5135
Wire Wire Line
	8215 5135 8290 5135
$Comp
L C_Small C17
U 1 1 566331C8
P 7290 5535
F 0 "C17" H 7315 5460 50  0000 L CNN
F 1 "10u" H 7315 5610 50  0000 L CNN
F 2 "multispeq:c_0603" H 7290 5535 10  0001 C CNN
F 3 "CC0603KRX5R6BB106" H 7290 5535 10  0001 C CNN
F 4 "CC0603KRX5R6BB106" H 7290 5535 60  0001 C CNN "manf#"
	1    7290 5535
	-1   0    0    1   
$EndComp
$Comp
L C_Small C20
U 1 1 5663369A
P 8090 5535
F 0 "C20" H 8115 5460 50  0000 L CNN
F 1 "10u" H 8115 5610 50  0000 L CNN
F 2 "multispeq:c_0603" H 8090 5535 10  0001 C CNN
F 3 "CC0603KRX5R6BB106" H 8090 5535 10  0001 C CNN
F 4 "CC0603KRX5R6BB106" H 8090 5535 60  0001 C CNN "manf#"
	1    8090 5535
	-1   0    0    1   
$EndComp
$Comp
L C_Small C23
U 1 1 56633A85
P 8790 4810
F 0 "C23" H 8815 4735 50  0000 L CNN
F 1 "10u" H 8815 4885 50  0000 L CNN
F 2 "multispeq:c_0603" H 8790 4810 10  0001 C CNN
F 3 "CC0603KRX5R6BB106" H 8790 4810 10  0001 C CNN
F 4 "CC0603KRX5R6BB106" H 8790 4810 60  0001 C CNN "manf#"
	1    8790 4810
	-1   0    0    1   
$EndComp
Wire Wire Line
	8815 4960 8815 5185
Wire Wire Line
	9115 4960 9140 4960
Wire Wire Line
	9015 5185 9240 5185
Connection ~ 9240 5185
Wire Wire Line
	8815 5185 8740 5185
Wire Wire Line
	7290 5235 7515 5235
Wire Wire Line
	7215 5235 7215 5485
Wire Wire Line
	7215 5485 7065 5485
Connection ~ 7065 5485
Wire Wire Line
	8040 4735 8490 4735
Wire Wire Line
	7065 5035 7290 5035
Wire Wire Line
	8490 5035 7840 5035
Text Notes 9590 4585 0    20   ~ 0
voltage verified 3/6/2017
Text Notes 7835 6385 0    60   ~ 0
LDL1117S50R
$Comp
L C_Small C8
U 1 1 59E36D55
P 7440 6780
F 0 "C8" H 7465 6705 50  0000 L CNN
F 1 "10u" H 7465 6855 50  0000 L CNN
F 2 "multispeq:c_0603" H 7440 6780 10  0001 C CNN
F 3 "CC0603KRX5R6BB106" H 7440 6780 10  0001 C CNN
F 4 "CC0603KRX5R6BB106" H 7440 6780 60  0001 C CNN "manf#"
	1    7440 6780
	-1   0    0    1   
$EndComp
$Comp
L C_Small C10
U 1 1 59E36FAC
P 8805 6780
F 0 "C10" H 8830 6705 50  0000 L CNN
F 1 "10u" H 8830 6855 50  0000 L CNN
F 2 "multispeq:c_0603" H 8805 6780 10  0001 C CNN
F 3 "CC0603KRX5R6BB106" H 8805 6780 10  0001 C CNN
F 4 "CC0603KRX5R6BB106" H 8805 6780 60  0001 C CNN "manf#"
	1    8805 6780
	-1   0    0    1   
$EndComp
$Comp
L AP1117-50 U3
U 1 1 59DF770E
P 8180 6680
F 0 "U3" H 8030 6805 50  0000 C CNN
F 1 "5V Linear Reg" H 8180 6805 50  0000 L CNN
F 2 "TO_SOT_Packages_SMD:SOT-223-3Lead_TabPin2" H 8180 6880 50  0001 C CNN
F 3 "LDL1117S50R" H 8280 6430 50  0001 C CNN
F 4 "LDL1117S50R" H 8180 6680 60  0001 C CNN "manf#"
	1    8180 6680
	1    0    0    -1  
$EndComp
Wire Wire Line
	8180 6980 8180 7100
Wire Wire Line
	7440 7100 9000 7100
Wire Wire Line
	8480 6680 8920 6680
Wire Wire Line
	8805 6880 8805 7100
Connection ~ 8805 7100
Connection ~ 8805 6680
Wire Wire Line
	7110 6680 7880 6680
Wire Wire Line
	7440 6880 7440 7100
Connection ~ 8180 7100
Connection ~ 7440 6680
$EndSCHEMATC
