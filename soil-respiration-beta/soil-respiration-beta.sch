EESchema Schematic File Version 4
LIBS:soil-respiration-beta-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L multispec:ITEAD_HC-05 U1
U 1 1 5D6D5EF0
P 4525 2400
F 0 "U1" H 4525 3550 60  0000 C CNN
F 1 "HC-06" H 4525 3444 60  0000 C CNN
F 2 "multispeq:HC_05_Bluetooth" H 4525 2400 60  0001 C CNN
F 3 "" H 4525 2400 60  0000 C CNN
	1    4525 2400
	1    0    0    -1  
$EndComp
$Comp
L soil-respiration-beta-rescue:Conn_01x14_Female-Connector J2
U 1 1 5D6D9AB0
P 2250 2525
F 0 "J2" V 2415 2455 50  0000 C CNN
F 1 "teensy bottom row" V 2324 2455 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x14_P2.54mm_Vertical" H 2250 2525 50  0001 C CNN
F 3 "~" H 2250 2525 50  0001 C CNN
	1    2250 2525
	0    -1   -1   0   
$EndComp
$Comp
L soil-respiration-beta-rescue:Conn_01x05_Female-Connector J4
U 1 1 5D6D9C89
P 2950 2275
F 0 "J4" H 2660 2035 50  0000 C CNN
F 1 "teensy side row" H 3275 2275 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x05_P2.54mm_Vertical" H 2950 2275 50  0001 C CNN
F 3 "~" H 2950 2275 50  0001 C CNN
	1    2950 2275
	-1   0    0    1   
$EndComp
$Comp
L soil-respiration-beta-rescue:Conn_01x14_Female-Connector J3
U 1 1 5D6D9F61
P 2350 2000
F 0 "J3" V 2422 1930 50  0000 C CNN
F 1 "teensy top row" V 2513 1930 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x14_P2.54mm_Vertical" H 2350 2000 50  0001 C CNN
F 3 "~" H 2350 2000 50  0001 C CNN
	1    2350 2000
	0    1    1    0   
$EndComp
$Comp
L soil-respiration-beta-rescue:Conn_01x04_Female-Connector J1
U 1 1 5D6DA2E0
P 1775 3225
F 0 "J1" V 1622 3373 50  0000 L CNN
F 1 "K30 co2 sensor UART Conn" V 1713 3373 50  0000 L CNN
F 2 "multispeq:K30_co2_sensor" H 1775 3225 50  0001 C CNN
F 3 "~" H 1775 3225 50  0001 C CNN
	1    1775 3225
	0    1    1    0   
$EndComp
Wire Wire Line
	1875 3025 1875 3000
Wire Wire Line
	1875 3000 2650 3000
Wire Wire Line
	2650 3000 2650 2725
Wire Wire Line
	1775 3025 1775 2900
Wire Wire Line
	1775 2900 2750 2900
Wire Wire Line
	1575 3025 1575 2975
Wire Wire Line
	1675 3025 1675 2900
Wire Wire Line
	1675 2900 1500 2900
Wire Wire Line
	1500 2900 1500 1800
Wire Wire Line
	1500 1800 1650 1800
Wire Wire Line
	4525 1450 3525 1450
Wire Wire Line
	3525 1450 3525 2375
Wire Wire Line
	3525 2375 3150 2375
Wire Wire Line
	4425 3350 4125 3350
Wire Wire Line
	3625 3350 3625 2275
Wire Wire Line
	3625 2275 3150 2275
Wire Wire Line
	4425 3350 4525 3350
Connection ~ 4425 3350
Connection ~ 4525 3350
Wire Wire Line
	4525 3350 4625 3350
Wire Wire Line
	3925 2050 3800 2050
Wire Wire Line
	3800 2050 3800 2850
Wire Wire Line
	3800 2850 1750 2850
Wire Wire Line
	3925 1950 3725 1950
Wire Wire Line
	3725 1950 3725 2800
Wire Wire Line
	3725 2800 1850 2800
NoConn ~ 1750 1800
NoConn ~ 1850 1800
NoConn ~ 1950 1800
NoConn ~ 2050 1800
NoConn ~ 2150 1800
NoConn ~ 2250 1800
NoConn ~ 2350 1800
NoConn ~ 2950 1800
NoConn ~ 2850 1800
NoConn ~ 2750 1800
NoConn ~ 2650 1800
NoConn ~ 2550 1800
NoConn ~ 2450 1800
NoConn ~ 3150 2075
NoConn ~ 3150 2175
NoConn ~ 3150 2475
NoConn ~ 2950 2725
NoConn ~ 2850 2725
NoConn ~ 2550 2725
NoConn ~ 2450 2725
NoConn ~ 2350 2725
NoConn ~ 2250 2725
NoConn ~ 2150 2725
NoConn ~ 2050 2725
NoConn ~ 1950 2725
NoConn ~ 5125 1700
NoConn ~ 5125 1800
NoConn ~ 5125 2000
NoConn ~ 5125 2100
NoConn ~ 5125 2200
NoConn ~ 5125 2300
NoConn ~ 5125 2400
NoConn ~ 5125 2500
NoConn ~ 5125 2600
NoConn ~ 5125 2700
NoConn ~ 5125 2800
NoConn ~ 5125 2900
NoConn ~ 5125 3000
NoConn ~ 5125 3100
NoConn ~ 3925 2650
NoConn ~ 3925 2750
NoConn ~ 3925 2900
NoConn ~ 3925 3000
NoConn ~ 3925 3100
NoConn ~ 3925 3200
NoConn ~ 3925 1850
NoConn ~ 3925 1750
NoConn ~ 3925 1600
$Comp
L soil-respiration-beta-rescue:GND-power #PWR0101
U 1 1 5D6E5D8C
P 4125 3350
F 0 "#PWR0101" H 4125 3100 50  0001 C CNN
F 1 "GND" H 4130 3177 50  0000 C CNN
F 2 "" H 4125 3350 50  0001 C CNN
F 3 "" H 4125 3350 50  0001 C CNN
	1    4125 3350
	1    0    0    -1  
$EndComp
Connection ~ 4125 3350
Wire Wire Line
	4125 3350 3625 3350
$Comp
L soil-respiration-beta-rescue:GND-power #PWR0102
U 1 1 5D6E5DC3
P 1575 2975
F 0 "#PWR0102" H 1575 2725 50  0001 C CNN
F 1 "GND" V 1580 2847 50  0000 R CNN
F 2 "" H 1575 2975 50  0001 C CNN
F 3 "" H 1575 2975 50  0001 C CNN
	1    1575 2975
	0    1    1    0   
$EndComp
Wire Wire Line
	1850 2800 1850 2725
Wire Wire Line
	1750 2850 1750 2725
Wire Wire Line
	2750 2900 2750 2725
Text Label 3525 1450 0    50   ~ 0
3.3V
Wire Wire Line
	1650 2725 1575 2725
Wire Wire Line
	1575 2725 1575 2975
Connection ~ 1575 2975
Text Label 1500 2275 0    50   ~ 0
Vin_3.3v
$EndSCHEMATC
